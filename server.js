// server.js

import cors from 'cors';
import express from 'express';
import next from 'next';
import passport from 'passport';
import swaggerJsdoc from 'swagger-jsdoc';
import url from 'url';
import apiLogin from './api/login';
import apiLogout from './api/logout';
import apiSignUp from './api/signup';
import apiUser from './api/user';
import { localStrategy } from './lib/password-local';

const dev = process.env.NODE_ENV !== 'production';
const port = process.env.PORT || 3000;
const app = next({ dev });

const handle = app.getRequestHandler();

const { parse } = url;

const options = {
  swaggerDefinition: {
    openapi: '3.0.0',
    info: {
      title: 'Swagger Example',
      version: '1.0.0',
    },
  },
  apis: ['./api/*.js', './models/user.js'],
};

const swaggerSpec = swaggerJsdoc(options);
passport.use(localStrategy);

app.prepare().then(() => {
  const server = express();
  server.use(cors());
  server.use(express.json());
  server.use(express.urlencoded({ extended: false }));
  server.use(passport.initialize());
  server.get('/api-docs.json', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    res.send(swaggerSpec);
  });
  server.all('/api/logout', apiLogout);
  server.post('/api/login', apiLogin);
  server.post('/api/signup', apiSignUp);
  server.all('/api/user', apiUser);

  server.all('*', (req, res) => {
    const parsedUrl = parse(req.url, true);
    return handle(req, res, parsedUrl);
  });
  server.listen(port, err => {
    if (err) throw err;
    console.log(`> Ready on http://localhost:${port}`);
  });
});
