import Link from 'next/link';

const Form = ({ isLogin, errorMessage, onSubmit }) => (
  <div className="container">
    <div className="row justify-content-center">
      <div className="card-group">
        <div className="card p-4">
          <div className="card-body">
            <form onSubmit={onSubmit}>
              {!isLogin && (
                <>
                  <div className="input-group mb-3">
                    <label>
                      <span>First Name</span>
                      <input className="form-control" type="text" name="first_name" required />
                    </label>
                  </div>
                  <div className="input-group mb-3">
                    <label>
                      <span>Last Name</span>
                      <input className="form-control" type="text" name="last_name" required />
                    </label>
                  </div>
                  <div className="input-group mb-3">
                    <label>
                      <span>Phone</span>
                      <input className="form-control" type="text" name="phone_number" required />
                    </label>
                  </div>
                </>
              )}
              <div className="input-group mb-3">
                <label>
                  <span>E-mail</span>
                  <input className="form-control" type="email" name="email" required />
                </label>
              </div>
              <div className="input-group mb-3">
                <label>
                  <span>Password</span>
                  <input className="form-control" type="password" name="password" required />
                </label>
              </div>
              {!isLogin && (
                <div className="input-group mb-3">
                  <label>
                    <span>Repeat password</span>
                    <input className="form-control" type="password" name="rpassword" required />
                  </label>
                </div>
              )}
              <div className="submit">
                {isLogin ? (
                  <>
                    <Link href="/signup">
                      <a>I don't have an account</a>
                    </Link>{' '}
                    <button className="btn btn-success" type="submit">
                      Login
                    </button>
                  </>
                ) : (
                  <>
                    <Link href="/login">
                      <a>I already have an account</a>
                    </Link>{' '}
                    <button className="btn btn-info" type="submit">
                      Signup
                    </button>
                  </>
                )}
              </div>
              {errorMessage && <p className="error">{errorMessage}</p>}
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default Form;
