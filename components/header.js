import Link from 'next/link';
import Router from 'next/router';
import { useUser } from '../lib/hooks';
const Header = () => {
  const user = useUser();

  async function onLogout(e) {
    e.preventDefault();

    try {
      const res = await fetch('/api/logout', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({}),
      });
      if (res.status === 204) {
        Router.push('/login');
      } else {
        throw new Error(await res.text());
      }
    } catch (error) {
      console.error('An unexpected error happened occurred:', error);
    }
  }

  return (
    <ul className="c-header-nav">
      <li className="c-header-nav-item px-3">
        <Link href="/">
          <a className="c-header-nav-link">Home</a>
        </Link>
      </li>
      {user ? (
        <>
          <li>
            <Link href="/profile">
              <a className="c-header-nav-link">Profile</a>
            </Link>
          </li>
          <li>
            <a className="c-header-nav-link" href="#logout" onClick={e => onLogout(e)}>
              Logout
            </a>
          </li>
        </>
      ) : (
        <li>
          <Link href="/login">
            <a className="c-header-nav-link">Login</a>
          </Link>
        </li>
      )}
    </ul>
  );
};

export default Header;
