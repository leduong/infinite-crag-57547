import Head from 'next/head';
import React, { useEffect, useRef, useState } from 'react';
import { SIDEBAR } from '../constants';
import Header from './header';

const Layout = props => {
  const [sidebarShow, setSidebarShow] = useState();
  const [darkMode, setDarkMode] = useState();
  const sidebarRef = useRef(null);

  useEffect(() => {
    const dark = window.matchMedia('(prefers-color-scheme: dark)').matches ? 'c-dark-theme' : '';
    setDarkMode(dark);

    document.addEventListener('click', handleClickOutside, true);
    return () => {
      document.removeEventListener('click', handleClickOutside, true);
    };
  }, []);

  const handleClickOutside = event => {
    if (sidebarRef.current && !sidebarRef.current.contains(event.target)) {
      setSidebarShow('');
    }
  };

  const sidebarMenu = () => {
    return SIDEBAR.map((ele, idx) => (
      <li className="c-sidebar-nav-item" key={idx}>
        <a className="c-sidebar-nav-link" href={'/' + ele.name.toLowerCase()} rel="dofollow">
          <b className="c-sidebar-nav-icon">
            <i className={`fa fa-${ele.icon}`}></i>
          </b>
          {ele.name}
        </a>
      </li>
    ));
  };

  const toggleSidebar = () => {
    // const val = 'c-sidebar-show' === sidebarShow ? '' : 'c-sidebar-show';
    // console.log(val);
    setSidebarShow('c-sidebar-show');
  };

  return (
    <>
      <Head>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <title> Test</title>
      </Head>

      <div className={`c-app c-default-layout ${darkMode}`}>
        <div
          ref={sidebarRef}
          className={`c-sidebar c-sidebar-dark c-sidebar-fixed ${sidebarShow}`}
          id="sidebar"
        >
          <ul className="c-sidebar-nav">{sidebarMenu()}</ul>
        </div>
        <div className="c-wrapper">
          <header className="c-header c-header-light c-header-fixed">
            <div className="c-header-brand mx-auto w-100">
              <button className="c-header-toggler ml-md-3" type="button" onClick={toggleSidebar}>
                <span className="c-header-toggler-icon"></span>
              </button>
              <Header />
            </div>
          </header>
          <div className="c-body c-default-layout">
            <main className="c-main">
              <div className="container-fluid">
                <div className="fade-in">
                  <main>{props.children}</main>
                </div>
              </div>
            </main>
          </div>
          <footer className="c-footer">
            <div>
              <small className="mr-1">Power by @leduong</small>
            </div>
            <div className="ml-auto">
              <small className="mr-1"> © 2020 Example</small>
            </div>
          </footer>
        </div>
      </div>
    </>
  );
};

export default Layout;
