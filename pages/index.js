import Layout from '../components/layout';
import { useUser } from '../lib/hooks';

const Home = () => {
  const user = useUser();

  return (
    <Layout>
      <h1>Example</h1>

      <p>Steps to test the example:</p>

      <ol>
        <li>Click Login and enter an username and password.</li>
        <li>
          You'll be redirected to Home. Click on Profile, notice how your session is being used
          through a token stored in a cookie.
        </li>
        <li>Click Logout and try to go to Profile again. You'll get redirected to Login.</li>
      </ol>

      {user && (
        <div>
          Currently logged in as: Your session:<pre>{JSON.stringify(user)}</pre>
        </div>
      )}
    </Layout>
  );
};

export default Home;
