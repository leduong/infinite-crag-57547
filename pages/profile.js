import Layout from '../components/layout';
import { useUser } from '../lib/hooks';

const Profile = () => {
  const user = useUser({ redirectTo: '/login' });

  return (
    <Layout>
      <h1>Profile</h1>
      {user && (
        <div className="card">
          <div className="card-body">
            Your session:<code>{JSON.stringify(user)}</code>
          </div>
        </div>
      )}
    </Layout>
  );
};

export default Profile;
