import Layout from '../components/layout';
import { useUser } from '../lib/hooks';

const Quest = () => {
  const user = useUser();

  return (
    <Layout>
      <h1>Quest Page - Under Construction</h1>

      {user && (
        <div>
          Currently logged in as: Your session:<pre>{JSON.stringify(user)}</pre>
        </div>
      )}
    </Layout>
  );
};

export default Quest;
