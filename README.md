# Example ReactJS + Swagger + API

## DEMO

https://ancient-depths-34457.herokuapp.com

## Swagger Docs

https://petstore.swagger.io/?url=https://ancient-depths-34457.herokuapp.com/api-docs.json

# Requirement

- Nodejs 10+ (npm@latest)
- SQL (Postgres, SQLite)

## Install package npm and build

```bash
npm install
npm run build
npm run migrate
npm start
```

### For development, default with port 3000 http://localhost:3000, change by process.env.PORT

```
$ export PORT=3000
$ npm run dev
```

## Start

Set Postgres with ENV Production, default use SQLite with ENV development/test

ex:

```
$ export NODE_ENV="production"
$ export DATABASE_URL="postgres://username:password@host:port/data"
// run migrate for new DB
$ npm run migrate
$ npm start
```
