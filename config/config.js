module.exports = {
  development: {
    dialect: 'sqlite',
    storage: './sqlite.db',
    operatorsAliases: 0,
  },
  test: {
    dialect: 'sqlite',
    storage: './sqlite.db',
    operatorsAliases: 0,
  },
  production: {
    url:
      'postgres://tixauczoxeiquv:a89ea09183468829dcd08eca1da71887d720302b8ec82c1159b242df0aad7820@ec2-52-206-44-27.compute-1.amazonaws.com:5432/d4bao7nukl0mki',
    dialect: 'postgres',
    operatorsAliases: 0,
  },
};
