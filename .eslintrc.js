module.exports = {
  env: {
    browser: true,
    es6: true,
  },
  extends: ['airbnb-base'],
  plugins: ['import', 'array-func'],
  globals: {},
  parserOptions: {
    ecmaVersion: 11,
    sourceType: 'module',
  },
  rules: {
    'import/no-cycle': 'warn',
    'no-unused-vars': ['error', { argsIgnorePattern: 'res|next|^err|reject' }],
    quotes: [
      2,
      'single',
      {
        avoidEscape: true,
        allowTemplateLiterals: true,
      },
    ],
  },
  ignorePatterns: ['/test/*.js', '**/node_modules/*.js', 'mocha.conf.js'],
};
