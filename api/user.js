import { getSession } from '../lib/iron';
/**
 * @swagger
 * /api/user:
 *   get:
 *     description: Return current user
 *     produces:
 *      - application/json
 *     responses:
 *       200:
 *         description: Get current user profile
 *         schema:
 *           type: array
 *           items:
 *             $ref: '#/definitions/User'
 */

export default async function user(req, res) {
  const session = await getSession(req);
  // After getting the session you may want to fetch for the user instead
  // of sending the session's payload directly, this example doesn't have a DB
  // so it won't matter in this case
  res.status(200).json({ user: session || null });
}
