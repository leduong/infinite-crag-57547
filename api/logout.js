import { removeTokenCookie } from '../lib/auth-cookies';

/**
 * @swagger
 *
 * /api/logout:
 *   get:
 *     produces:
 *       - application/json
 */
export default async function logout(_req, res) {
  await removeTokenCookie(res);
  return res.status(204).end();
}
