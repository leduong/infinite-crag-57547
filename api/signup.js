/**
 * @swagger
 *
 * /api/signup:
 *   post:
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     responses:
 *       405:
 *         description: "Invalid input"
 *       201:
 *         schema:
 *           description: Create User is success
 *           type: object
 *           $ref: '#/definitions/User'
 *     requestBody:
 *       description: User Model for Sign up
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/definitions/User'
 */

import bCrypt from 'bcrypt-nodejs';
import { setTokenCookie } from '../lib/auth-cookies';
import { encryptSession } from '../lib/iron';
import db from '../models';
const User = db.User;

export default async (req, res) => {
  const { body } = req;
  const generateHash = function (password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(8), null);
  };

  try {
    await User.findOne({ where: { email: body?.email } }).then(user => {
      if (user) {
        return res.status(405).json({
          message: 'That email is already taken',
        });
      } else {
        const userPassword = generateHash(body.password);
        const data = {
          ...body,
          password: userPassword,
        };

        User.create(data).then((newUser, _created) => {
          if (!newUser) {
            return res.status(405).end();
          }

          if (newUser) {
            delete newUser.password;
            const token = async () => await encryptSession(newUser);
            setTokenCookie(res, token);
            return res.status(201).json(newUser);
          }
        });
      }
    });
  } catch (error) {
    console.error(error);
    res.status(500).end(error.message);
  }
};
