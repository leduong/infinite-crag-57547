import bCrypt from 'bcrypt-nodejs';
import { setTokenCookie } from '../lib/auth-cookies';
import { encryptSession } from '../lib/iron';
import db from '../models';
/**
 * @swagger
 *
 * /api/login:
 *   post:
 *     produces:
 *       - application/json
 *     requestBody:
 *       description: User for Sign un
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/definitions/Login'
 */

/**
 * @swagger
 * definitions:
 *   Login:
 *     type: object
 *     properties:
 *       email:
 *         type: string
 *       password:
 *         type: string
 */

const User = db.User;

const isValidPassword = (userpass, password) => {
  return bCrypt.compareSync(password, userpass);
};

export default async (req, res) => {
  const { body } = req;
  try {
    await User.findOne({ where: { email: body?.email } })
      .then(user => {
        if (!user) {
          return res.status(405).json({ message: 'Email does not exist' });
        }

        if (!isValidPassword(user.password, body.password)) {
          return res.status(405).json({ message: 'Incorrect password.' });
        }

        (async () => {
          const userinfo = user.get();
          delete userinfo.password;
          const token = await encryptSession(userinfo);
          setTokenCookie(res, token);
          return res.status(200).json({ ...userinfo, token });
        })();
      })
      .catch(function (err) {
        console.log('Error:', err);
        return res.status(500).json({
          message: 'Something went wrong with your Signin',
        });
      });
  } catch (error) {
    console.error(error);
    res.status(401).send(error.message);
  }
};
