module.exports = (sequelize, DataTypes) =>
  sequelize.define(
    'User',
    {
      first_name: DataTypes.STRING(16),
      last_name: DataTypes.STRING(16),
      phone_number: DataTypes.STRING(12),
      email: DataTypes.STRING(128),
      password: DataTypes.STRING(64),
      photo_url: DataTypes.STRING(2000),
      address_1: DataTypes.STRING(255),
      address_2: DataTypes.STRING(255),
      city: DataTypes.STRING(64),
      state: DataTypes.STRING(64),
      zipcode: DataTypes.STRING(10),
      country: DataTypes.STRING(64),
    },
    {
      timestamps: true,
      underscored: true,
      tableName: 'user',
    },
  );

/**
 * @swagger
 * definitions:
 *   User:
 *     type: object
 *     properties:
 *       first_name:
 *         type: string
 *       last_name:
 *         type: string
 *       phone_number:
 *         type: string
 *       password:
 *         type: string
 *       email:
 *         type: string
 *       address_1:
 *         type: string
 *       address_2:
 *         type: string
 *       city:
 *         type: string
 *       state:
 *         type: string
 *       zipcode:
 *         type: string
 *       country:
 *         type: string
 *       photo_url:
 *         type: string
 */
