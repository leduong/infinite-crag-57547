export const SIDEBAR = [
  {
    name: 'Home',
    icon: 'home',
    sub: [],
  },
  {
    name: 'Profile',
    icon: 'user',
    sub: [],
  },
  {
    name: 'Quest',
    icon: 'arrow-circle-right',
    sub: [],
  },
  {
    name: 'Logout',
    icon: 'sign-out',
    sub: [],
  },
];
