const isProd = process.env.NODE_ENV === 'production';
const path = require('path');

module.exports = {
  optimization: {
    minimize: isProd,
  },
  compress: isProd,
  sassOptions: {
    includePaths: [path.join(__dirname, 'scss')],
  },
};
